from functools import reduce
from itertools import combinations


targets = []
target = 2020
with open("puzzle_input.txt") as f:
    content = f.readlines()

expenses = [int(x.strip()) for x in content]
for pair in combinations(expenses, 2):
    if sum(pair) == target:
        targets.extend(pair)

print(reduce(lambda x, y: x * y, targets))
