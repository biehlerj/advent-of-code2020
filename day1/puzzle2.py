from functools import reduce
from itertools import combinations

targets = []
target = 2020
with open("puzzle_input.txt") as f:
    content = f.readlines()

expenses = [int(x.strip()) for x in content]
for triplets in combinations(expenses, 3):
    if sum(triplets) == target:
        targets.extend(triplets)

print(targets)
print(reduce(lambda x, y: x * y, targets))
