# Advent of Code 2020

Advent of Code is a month long challenge of programming puzzles that help exercise a variety of skills that can be solved in any programming language.

## Table of Contents


<!-- vim-markdown-toc GitLab -->

* [Introduction](#introduction)
* [Directory Structure](#directory-structure)
* [Author](#author)

<!-- vim-markdown-toc -->

## Introduction

Advent of Code is a yearly challenge to solve programming puzzles in the programming language of your choice in order to help hone and develop skills and exercise your problem solving skills. For 2020 I chose to use Python in order to level up my Python skills and focus more on being able to solve the exercises without worrying about learning a new language at the same time.

## Directory Structure

Each day will have it's own directory that will have at least 3 files (possibly 4 if I need to use more input files). The structure will be:

```
dayNumber/
    puzzle1.py
    puzzle2.py
    puzzle_input
    puzzle_input2 (if necessary)
```

The input files will be formatted in a filetype that is beneficial to solving the problem.

## Author

[Jacob Biehler](https://www.linkedin.com/in/jacob-biehler-475573139/)

[Twitter](https://twitter.com/Biehlerj)

[GitLab](https://gitlab.com/biehlerj)
